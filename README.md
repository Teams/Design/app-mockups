# GNOME App Mockups

This repository contains mockups and concepts for GNOME  core apps. Since we don't want it to become too large in terms of filesize, it should only include apps with a reasonably small number of mockups. More complex apps like GNOME Software should be in their own repositories, and smaller non-core apps should go [here](https://gitlab.gnome.org/Teams/Design/other-app-mockups).

# For Developers

**Design materials in this repository should be considered non-final or experimental.** Mockups should not implemented as-seen unless they are presented directly, via a ticket against a particular project or other means, for implementation. If you are looking to implement some of these concepts for a GNOME module reach out to the [Design Team](https://gitlab.gnome.org/Teams/Design/) about interest in doing so we can be of assistance.

# Other Mockups

The following repositories contain other GNOME mockups:
- [App Mockups](https://gitlab.gnome.org/Teams/Design/app-mockups) (this repo): Core apps, if they're not too complex
- [Settings Mockups](https://gitlab.gnome.org/Teams/Design/settings-mockups)
- [Software Mockups](https://gitlab.gnome.org/Teams/Design/software-mockups)
- [Other App Mockups](https://gitlab.gnome.org/Teams/Design/other-app-mockups): Non-core apps
- [OS Mockups](https://gitlab.gnome.org/Teams/Design/os-mockups): Shell, design patterns, and other system level stuff
